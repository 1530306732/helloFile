#include "server.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

const char init_page[] = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"></head><body>Hello World</body>";

Package unique_package;// 保存client的独有网页
int main() {
	int sock = createSock_srv(); // 设置sock，默认bind INADDR_ANY
	if (sock<=0) {
	  printf("failed to create listened sock\n");
	  exit(0);
	}
	unique_package.size = strlen(init_page);
	unique_package.block = malloc(unique_package.size);
	memcpy(unique_package.block, init_page, unique_package.size); 
	queue *Q = (queue*) malloc(sizeof(queue)); // 申请一个队列保存收集的package
	queue_init(Q, 20);
	while(1) {
		int client_sock = waitConnect(sock); // 等待client连接
		char flag = readHello(client_sock);  // 判断client发送的头
		if (flag==PULLCHAR) {
			printf("Read request : PULL \n"); 
			sendPackage( client_sock, queue_pop(Q) ); // 发包
		} else if (flag==PUSHCHAR) {
			printf("Read request : PUSH \n"); 
			queue_push( Q, recvPackage(client_sock) ); // 收包
		} else if (flag==PUSHVCHAR) { //接收client的独有网页, , @PUSV|size|block
			printf("Read request : PUSHV \n"); 
			if(unique_package.block) free(unique_package.block); // 释放旧包
			unique_package.size = 0;
		    unique_package = recvPackage(client_sock);
		} else if (flag==PULLVCHAR) { //正常读取最新网页
		    printf("Read request : PULLV \n"); 
			char buf[1024] = {0};
		    sprintf(buf, "HTTP/1.0 200 OK\r\n");    //line:netp:servestatic:beginserve
			sprintf(buf, "%sServer: helloVis\r\n", buf);
			sprintf(buf, "%sContent-length: %d\r\n", buf, unique_package.size);
			sprintf(buf, "%sContent-type: %s\r\n\r\n", buf, "text/html");
			send(client_sock, buf, strlen(buf), 0);
			sendBlock(client_sock, unique_package.block, unique_package.size);
		} else {
			printf("Unknown request\n");
		}
		close(client_sock);
	}
	close(sock);
}
