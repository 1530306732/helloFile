import csv
import sys

head = '''
<!DOCTYPE html>
<html>
<head>
    <title>Basic Table Format</title>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
'''

tail = '''
</body>
</html>
'''
def csv_to_html_table(csv_file, html_file):
    # 读取CSV文件
    with open(csv_file, 'r', newline='') as csvfile:
        reader = csv.reader(csvfile)
        # 获取表头
        headers = next(reader, None)

        # 创建HTML表格的开头和结尾
        html_table = ['<table>']
        html_table.append('<thead>')
        for h in headers:
            html_table.append('<th>{}</th>'.format(h))
        html_table.append('</thead>')
        html_table.append('<tbody>')

        # 遍历CSV文件的每一行，并将其转换为HTML表格的行
        for row in reader:
            html_table.append('<tr>')
            for cell in row:
                html_table.append('<td>{}</td>'.format(cell))
            html_table.append('</tr>')

        # 添加HTML表格的结尾
        html_table.append('</tbody>')
        html_table.append('</table>')

    # 将HTML表格写入文件
    with open(html_file, 'w') as htmlfile:
        htmlfile.write(head) 
        htmlfile.write('\n'.join(html_table))
        htmlfile.write(tail) 

# 使用示例
def main(csv_file_path , html_file_path):  # HTML文件路径
    csv_to_html_table(csv_file_path, html_file_path)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage: {} csv_file.csv html_file.html'.format(sys.argv[0]))
        exit(0)
    main(sys.argv[1], sys.argv[2])
