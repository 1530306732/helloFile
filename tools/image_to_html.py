import base64
from PIL import Image
from io import BytesIO

def png_to_base64(image_path):
    # 打开图片并转换为RGB模式
    with Image.open(image_path) as img:
        rgb_im = img.convert('RGB')

    # 使用BytesIO对象将图片转换为字节流
    output = BytesIO()
    rgb_im.save(output, format='PNG')

    # 使用base64对字节流进行编码
    encoded_string = base64.b64encode(output.getvalue()).decode('utf-8')

    return encoded_string

def image_to_html(image_path):
    # 使用函数并打印结果
    base64_string = png_to_base64(image_path)
    return '''
    <!DOCTYPE html>
<html>
<head>
  <title>Blob Image Display</title>
</head>
<body>
  <h1>Blob Image Display</h1>

  <img id="blobImage" src="data:image/png;base64,{}" alt="Blob Image">

</body>
</html>
    '''.format(base64_string)

# 使用函数并打印结果
base64_string = image_to_html('demo.png')
print(base64_string)