#define _WIN32_WINNT 0x0501 // for useof getaddrinfo and freeaddrinfo
#include <errno.h>
#include <WinSock2.h>
#include <windows.h>
#include <WS2tcpip.h>
#include <IPHlpApi.h>
#include <stdio.h>
#include <time.h>
#pragma comment(lib, "Ws2_32.lib")
#include "config.h"
typedef unsigned int uint32_t;
const char PUSHCHAR = '1';
const char PULLCHAR = '0';
const int MAXSIZE = 2147483640; // Set max size of file to 300M

void sendInt(int sock, int a) {
	int b = htonl(a);
	int nwrite = send(sock, (void*)&b, sizeof(int), 0);
	if( nwrite== -1 ) {
		if(EINTR == errno || EAGAIN == errno ) {
			nwrite = 0;
		} else {
			printf("%s,%d, Send() -1, 0x%x\n", __FILE__, __LINE__, errno);
			return;
		}
	}
}

int sayHello( int sock, char c ) {
	char s[100];
	if(c==PUSHCHAR) 
		strcpy(s,"@PUSH");
	else
		strcpy(s,"@PULL");
	return send(sock, s, strlen(s), 0);
}

/*
void sendChar(int sock, char c) {
	int nwrite = send(sock, (void*)&c, sizeof(char), 0);
	if( nwrite== -1 ) {
		if(EINTR == errno || EWOULDBLOCK == errno || EAGAIN == errno ) {
			nwrite = 0;
		} else {
			printf("%s,%d, Send() -1, 0x%x\n", __FILE__, __LINE__, errno);
			return;
		}
	}
}
*/

const int MSG_DONTWAIT = 0;
typedef struct _fdata {
	uint32_t nameSize;
	uint32_t dataSize;
} fdata;

static int receiveInt(int client_sock) {
	int n;
	recv(client_sock, (void*)&n, sizeof(int),0);
	return ntohl( n );
}

static void* readBlock(int client_sock, int len) {
		if(len == 0) return NULL;
        void *buffer = malloc(len);
        void *p = buffer;
        while(len>0) {
                int rsize = recv(client_sock, p, len, 0);
                len-=rsize;
                p+=rsize;
        }
        return buffer;
}

int waitConnect( int sock ) {
	struct sockaddr_in client_addr;
	int client_addr_size=sizeof(struct sockaddr); // in some case we need to give hint length? Or it will fail
	int client_sock = -1;
	client_sock = accept(sock, (struct sockaddr*)&client_addr, &client_addr_size);
	if( client_sock < 0 ) {
		printf("Error at socket(): %ld\n", WSAGetLastError());
		WSACleanup();
		exit(0);
	}

	char buffer[1024];
	char buffer2[2024];
	//inet_ntop(AF_INET, &client_addr.sin_addr, buffer, sizeof(buffer));
	sprintf(buffer2," visitor: %s",buffer);
	//Log(buffer2);
	return client_sock;
}

char readHello( int client_sock ) {
	char s[100]={0};
	time_t pre = time(NULL);
	while(1) {
		int t = recv(client_sock, s, 5, MSG_DONTWAIT);
		if(t>0) 
			break;
		else {
			time_t now = time(NULL);
			if(now-pre>3) // wait at most 3 seconds
				break;
		}
	}
	if(strcmp(s, "@PUSH")==0) return PUSHCHAR;
	else if(strcmp(s,"@PULL")==0) return PULLCHAR;
	else return '2';
}

typedef struct {
	int size;
	void* block;
} Package;

typedef struct _queue {
	int l;
	int r;
	int size;
	int capacity;
	Package* data;
} queue;

void queue_init(queue*q, int n) {
	q->data = malloc(n*sizeof(Package));
	q->l = 0;
	q->r = -1;
	q->size = 0;
	q->capacity = n;
}

int queue_push(queue* q, Package p) {
	if(q->size >= q->capacity) return -1; // full
	++q->r;
	if(q->r >= q->capacity) q->r%=q->capacity;
	q->data[q->r] = p;
	q->size++;
	return 0;
}

Package queue_pop(queue* q) {
	if(q->size==0) {
		Package a;
		a.size = 0;
		a.block = NULL;
		return a;
	}
	Package ans = q->data[q->l];
	q->l++;
	if(q->l >= q->capacity) q->l%=q->capacity;
	q->size--;
	return ans;
}

void queue_release(queue* q) {
	free(q->data);
}

void helper() {
	printf("Useage : pushFile file1 [file2, [file3..]] \n");
}

int open_listenedFd(const char* port) {
	WSADATA wsaData;
	int iResult;
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0)
	{
		printf("WSAStartup failed: %d\n", iResult);
		return -1;
	}
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		   *ptr = NULL,
		   hints;
	ZeroMemory( &hints, sizeof(hints) );
	hints.ai_family = AF_INET;  //可以是IPv4或IPv6地址
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	
	iResult = getaddrinfo("0.0.0.0", port, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed: %d\n", iResult);
		WSACleanup();
		return -1;
	}

	ptr = result;
	// create socket
	ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, 
		ptr->ai_protocol);
	if (ConnectSocket == INVALID_SOCKET) {
		printf("Error at socket(): %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return -1;
	}

	iResult = bind( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		closesocket(ConnectSocket);
		ConnectSocket = INVALID_SOCKET;
		printf("bind failed \n");
		return -1;
	}

	listen(ConnectSocket, 20);
	return ConnectSocket;
}

int sendBlock(int sock, void*block, int  len) {
	// some work may do for optimization
	while(len>0) {
		int wsize = send(sock, block, len, 0);
		len -= wsize;
		block += wsize;
	}
	return 0;
}


void sendPackage( int sock, Package p ) {
	printf("sending %d bytes\n", p.size);
	sendInt(sock, p.size);
	if( p.size!=0 )
	{
		sendBlock(sock, p.block, p.size);
		free(p.block); 
	}
}

void Log(const char*s) {
	printf("%s\n", s);
}

Package recvPackage( int sock) {
	Package ans;
	ans.size = receiveInt(sock);
	if(ans.size <= 0) {
		Log("Error : package with negetive size ignored!\n");
		ans.size = 0;
		ans.block = NULL;
		return ans;
	} else if( ans.size> MAXSIZE) {
		Log("Error : package with too large size ignored!\n");
		ans.size = 0;
		ans.block = NULL;
		return ans;
	}
	//printf("receiving %d bytes data\n",ans.size);
	ans.block = readBlock(sock, ans.size);
	//printf("received %d bytes data\n",ans.size);
	return ans;
}

int main(int argc, char** argv)
{
	int sock = open_listenedFd(portstr);
	if (sock<=0) {
	  printf("failed to create listened sock\n");
	  exit(0);
	}
	queue *Q = (queue*) malloc(sizeof(queue)); // 申请一个队列保存收集的package
	queue_init(Q, 20);
	while(1) {
		int client_sock = waitConnect(sock); // 等待client连接
		char flag = readHello(client_sock);  // 判断client发送的头
		if (flag==PULLCHAR) {
			printf("Read request : PULL \n"); 
			sendPackage( client_sock, queue_pop(Q) ); // 发包
		} else if (flag==PUSHCHAR) {
			printf("Read request : PUSH \n"); 
			queue_push( Q, recvPackage(client_sock) ); // 收包
		} else {
			printf("Unknown request\n");
		}
		closesocket(client_sock);
	}
	closesocket(sock);
	return 0;
}

