#include "client.h"

int main(int argc, char** argv) {
	char flag = dealWithInput( argc, argv); //从输入判断是pull还是push
	int sock = createSock_client(); // 连接到服务器的sock, 需要读配置文件
	if (sock<=0) {
	  printf("failed to create connected sock\n");
	  exit(0);
	}
	if ( flag == PUSHCHAR ) {
		Package p = tar(argc-2, argv+2); // 将要发送的内容打包
		if(p.size == 0) return 0; // 空包不发送
		sayHello(sock, PUSHCHAR); // 先发给招呼, 要push
		sendPackage(sock, p); // 发包
		printf("------- push over ------- \n");
	} else if (flag==PULLCHAR) {
		sayHello(sock, PULLCHAR); // 先发给招呼，要pull
		Package p = recvPackage(sock); // 收包
		untar(p); // 解压包，进一步处理
		//printf("------- pull over ------- \n");
	} else if (flag==PUSHVCHAR) {
		Package p = tar_vis(argc-2, argv+2); // 将要发送的内容打包
		if(p.size == 0) return 0; // 空包不发送
		sayHello(sock, PUSHVCHAR); // 先发给招呼, 要pushv
		sendPackage(sock, p); // 发包
		printf("------- push over ------- \n");
	}
	close(sock);
}
